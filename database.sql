-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Lun 18 Septembre 2017 à 15:07
-- Version du serveur :  5.5.42
-- Version de PHP :  5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `api_php`
--

-- --------------------------------------------------------

--
-- Structure de la table `tune`
--

CREATE TABLE `tune` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tune`
--

INSERT INTO `tune` (`id`, `name`, `duration`) VALUES
(1, 'johnny hallyday', '00:05:01'),
(2, 'the beatles', '00:03:24'),
(3, 'Daft punk', '00:09:15');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`) VALUES
(1, 'stef', 'stef@mail.com'),
(2, 'deezer', 'deezer@mail.com');

-- --------------------------------------------------------

--
-- Structure de la table `user_tune`
--

CREATE TABLE `user_tune` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tune` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_tune`
--

INSERT INTO `user_tune` (`id`, `id_user`, `id_tune`) VALUES
(8, 1, 1),
(9, 1, 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `tune`
--
ALTER TABLE `tune`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_tune`
--
ALTER TABLE `user_tune`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_id_tune_id_user` (`id_tune`,`id_user`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_tune` (`id_tune`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `tune`
--
ALTER TABLE `tune`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `user_tune`
--
ALTER TABLE `user_tune`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `user_tune`
--
ALTER TABLE `user_tune`
  ADD CONSTRAINT `C_user_tune_tune` FOREIGN KEY (`id_tune`) REFERENCES `tune` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `C_user_tune_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
