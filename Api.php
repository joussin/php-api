<?php

// ROUTER and REQUEST MANAGEMENT
class Api {

  //API_KEY_AUTHORIZED
  private $API_KEYS_AUTHORIZED = [
    'c3RlZjoxMjMxMjM=', // stef:123123 in base64
    'ZGVlemVyOjEyMzEyMw==', // deezer:123123 in base64
  ];
  // index.php/path
  private $path;
  // GET POST PUT DELETE etc...
  private $method;
  // index.php/path?query=value
  private $query;
  // all request headers
  private $request_headers;

  public function __construct() {
    // DOC PHP : http://php.net/manual/fr/reserved.variables.server.php
    // $_SERVER["PATH_INFO"]
    // Contient les informations sur le nom du chemin fourni par le client concernant le nom du fichier
    // exécutant le script courant, sans la chaîne relative à la requête si elle existe. Actuellement, si
    // le script courant est exécuté via l'URL http://www.example.com/php/path_info.php/some/stuff?foo=bar,
    // alors la variable $_SERVER['PATH_INFO'] contiendra /some/stuff.

    $this->request_headers = getallheaders();

    $this->path = $_SERVER["PATH_INFO"];
    $this->method = $_SERVER["REQUEST_METHOD"];
    $this->query = $_SERVER["QUERY_STRING"];

  }

  // Get params from query string for GET method
  // OR params from header as params : JSON_STRING ( exemple: params = {"id":1,"text":"content text"})  for POST PUT DELETE and OTHERS methods
  private function getParams() {
    switch ($this->method) {
      case 'GET':
      return $this->getQueryParams();
      break;
      case 'POST':
      return $this->getRequestParams();
      break;
      case 'PUT':
      return $this->getRequestParams();
      break;
      case 'DELETE':
      return $this->getRequestParams();
      break;
      default:
      return $this->getRequestParams();
      break;
    }
  }

  // PARAMS for GET
  private function getQueryParams() {
    return (isset($_GET) && !empty($_GET)) ? $_GET : [];
  }

  // PARAMS for POST PUT DELETE and others ...
  private function getRequestParams() {
    if ( isset($this->request_headers) && !isset($this->request_headers["params"]) ) {
      return [];
    }
    $params = json_decode($this->request_headers["params"]);
    return $params != NULL ? $params : [];
  }

  private function checkAuthorization(){
    if ( empty($this->request_headers["Authorization"]) ) {
      return self::Unauthorized();
      exit;
    } else {
      $token =  $this->request_headers["Authorization"];
      if( !in_array($token,$this->API_KEYS_AUTHORIZED) ){
        return self::Unauthorized();
        exit;
      }
    }
  }

  // RETURN Callback or Response if 404
  public function request($path,$method,$next,$callback){

    $this->checkAuthorization();

    if ( $path == $this->path && $method == $this->method) {
      $params = $this->getParams();
      $callback($params);
      return;
    }
    else if (!$next) {
      $message = Response::$messages[Response::HTTP_NOT_FOUND];
      $content = json_encode(["code" => Response::HTTP_NOT_FOUND, "message"=>$message]);
      return Response::send(Response::HTTP_NOT_FOUND,"application/json",$content);
    }
  }

  public static function badRequest(){
    $message = Response::$messages[Response::HTTP_BAD_REQUEST];
    $content = json_encode(["code" => Response::HTTP_BAD_REQUEST, "message"=>$message]);
    return Response::send(Response::HTTP_BAD_REQUEST,"application/json",$content);
  }

  public static function Unauthorized(){
    $message = Response::$messages[Response::HTTP_UNAUTHORIZED];
    $content = json_encode(["code" => Response::HTTP_UNAUTHORIZED, "message"=>$message]);
    return Response::send(Response::HTTP_UNAUTHORIZED,"application/json",$content);
  }



}
