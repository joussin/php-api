<?php


class Database {

  private static $_instance = null;

  public $connection;

  private function __construct() {
    $this->connection = $this->getConnection();
  }

  public static function getInstance() {
    if(is_null(self::$_instance)) {
      self::$_instance = new Database();
    }
    return self::$_instance;
  }

  public function getConnection(){
    return new PDO('mysql:host='.DATABASE_HOST.';dbname='.DATABASE_NAME,
    DATABASE_USER,
    DATABASE_PASS,
    [
      PDO::ATTR_PERSISTENT => true
    ]);
  }

  // -----------   MODELS FCT --------------------

  // MODEL USER
  function getUser($id) {
    $result = $this->connection->prepare( 'SELECT * FROM `user` WHERE `id`= :id' );
    $result->bindValue( 'id', $id, PDO::PARAM_INT );
    $result->execute();
    $users = $result->fetchAll();
    if (count($users) == 1) {
      $user = $users[0];
      return  new User($user['id'],$user['username'],$user['email']);
    } else {
      return false;
    }
  }

  // MODEL TUNE
  function getTune($id) {
    $result = $this->connection->prepare( 'SELECT * FROM `tune` WHERE `id`= :id' );
    $result->bindValue( 'id', $id, PDO::PARAM_INT );
    $result->execute();
    $tunes = $result->fetchAll();
    if (count($tunes) == 1) {
      $tune =   $tunes[0];
      return  new Tune($tune['id'],$tune['name'],$tune['duration']);
    } else {
      return false;
    }
  }

  // JOIN USER tune
  function getTunesByUser($id) {
    $tunesFinal = [];

    $result = $this->connection->prepare( 'SELECT t.id,t.name,t.duration FROM `tune` AS t JOIN `user_tune` AS ut ON ut.id_tune = t.id JOIN `user` AS u ON u.id = ut.id_user WHERE `u`.id = :id' );
    $result->bindValue( 'id', $id, PDO::PARAM_INT );
    $result->execute();
    $tunes = $result->fetchAll();

    foreach ($tunes as $tune) {
      array_push($tunesFinal,new Tune($tune['id'],$tune['name'],$tune['duration']));
    }
    return $tunesFinal;
  }

  // add tune to user
  function addTuneToUser($tune,$user) {

    $result = $this->connection->prepare( 'INSERT INTO user_tune (id_user,id_tune) VALUES (:id_user, :id_tune)' );
    $result->bindValue( 'id_user', $user, PDO::PARAM_INT );
    $result->bindValue( 'id_tune', $tune, PDO::PARAM_INT );
    $result = $result->execute();

    if ($result)
    return true;
    else
    return false;
  }

  // remove tune from user
  function removeTuneFromUser($tune,$user) {
    $result = $this->connection->prepare( 'DELETE FROM user_tune  WHERE id_user=:id_user AND  id_tune=:id_tune' );
    $result->bindValue( 'id_user', $user, PDO::PARAM_INT );
    $result->bindValue( 'id_tune', $tune, PDO::PARAM_INT );
    $result = $result->execute();

    if ($result)
    return true;
    else
    return false;
  }


}
