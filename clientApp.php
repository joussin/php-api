<?php

define("DATABASE_HOST","localhost");
define("DATABASE_PORT","8889");
define("DATABASE_NAME","api_php");
define("DATABASE_USER","root");
define("DATABASE_PASS","root");

require_once 'Database.php';


require_once 'User.php';
require_once 'Tune.php';


require_once 'Response.php';
require_once 'Api.php';


$database = Database::getInstance();


// ______________ API ____________________
//
//
$app = new Api();

/*
GET /user?id=1 HTTP/1.1
Host: localhost
Authorization: ZGVlemVyOjEyMzEyMw==
*/
$app->request("/user","GET",true,function($params) use ($database) {
  if(!isset($params['id'])){
    return Api::badRequest();
  }
  $user = $database->getUser($params['id']);
  $content = ($user) ? $user : ["error" => "impossible de recupérer cet utilisateur"];
  return Response::send(Response::HTTP_OK,"application/json",json_encode($content));
});

/*
GET /tune?id=1 HTTP/1.1
Host: localhost
Authorization: ZGVlemVyOjEyMzEyMw==
*/
$app->request("/tune","GET",true,function($params) use ($database) {
  if(!isset($params['id'])){
    return Api::badRequest();
  }
  $tune = $database->getTune($params['id']);
  $content = ($tune) ? $tune : ["error" => "impossible de recupérer ce morceau"];
  return Response::send(Response::HTTP_OK,"application/json",json_encode($content));
});

/*
GET /user/tunes?id=1 HTTP/1.1
Host: localhost
Authorization: ZGVlemVyOjEyMzEyMw==
*/
$app->request("/user/tunes","GET",true,function($params) use ($database) {
  if(!isset($params['id'])){
    return Api::badRequest();
  }
  $tunes = $database->getTunesByUser($params['id']);
  return Response::send(Response::HTTP_OK,"application/json",json_encode($tunes));
});

/*

POST /user/tune HTTP/1.1
Host: localhost
params: {"id_user":1,"id_tune":1}
Content-Type: multipart/form-data;
Authorization: ZGVlemVyOjEyMzEyMw==
*/
$app->request("/user/tune","POST",true,function($params) use ($database) {
  if(!isset($params->id_tune) || !isset($params->id_user) ){
    return Api::badRequest();
  }
  $success = $database->addTuneToUser($params->id_tune,$params->id_user);
  $content = $success ? ["success" => "Ce morceau est ajouté à cet utilisateur !"] : ["error" => "impossible de d'ajouter ce morceau à cet utilisateur"];
  return Response::send(Response::HTTP_OK,"application/json",json_encode($content));
});

/*
DELETE /user/tune HTTP/1.1
Host: localhost
params: {"id_user":1,"id_tune":1}
Content-Type: multipart/form-data;
Authorization: ZGVlemVyOjEyMzEyMw==
*/
$app->request("/user/tune","DELETE",false,function($params) use ($database) {
  if(!isset($params->id_tune) || !isset($params->id_user) ){
    return Api::badRequest();
  }
  $success = $database->removeTuneFromUser($params->id_tune,$params->id_user);
  $content = $success ? ["success" => "Ce morceau est retiré à cet utilisateur !"] : ["error" => "impossible de retirer ce morceau à cet utilisateur"];
  return Response::send(Response::HTTP_OK,"application/json",json_encode($content));
});
